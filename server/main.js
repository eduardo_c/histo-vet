var express = require('express');
var app = express();
var server = require('http').Server(app);
var bodyParser = require('body-parser');
var methodOverride = require("method-override");
var path = require('path')
const nodeMailer = require("nodemailer");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

app.use(express.static('public'));

/*var route = express.Router();
route.post("/dating_request", function(req, res){
  console.log("Iniciando solicitud de cita");
  var send = require("gmail-send")({
    user: ""
  });
});*/

var router = express.Router();

router.post("/service_request", function(req, res){
  // create reusable transporter object using the default SMTP transport
  var transporter = nodeMailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'histovet.test@gmail.com',
      pass: 'histovet'
    }
  });

  // setup email data with unicode symbols
  var mailOptions = {
      from: '"Histovet Web Site" <histovet.test@gmail.com>', // sender address
      to: 'edu.castro.jecc@gmail.com', // list of receivers
      subject: 'Notificaciones Histovet Usuario: ' + req.body.userName, // Subject line
      html:
      "<p><b>Nombre del Cliente: </b></p>" + req.body.userName + "<br>" +
      "<p><b>Correo: </b></p>" + req.body.userEmail + "<br>" +
      "<p><b>Teléfono: </b></p>" + req.body.userPhone + "<br>" +
      "<p><b>Fecha requerida para el servicio: </b></p>" + req.body.date + "<br>" +
      "<p><b>Servicio solicitado por cliente: </b></p>" +
      "<p>" +
        req.body.st + " " + req.body.st2 + " " + (req.body.st3 != undefined ? req.body.st3 : " ") + // plain text body
        "</p><br>...<br>" +
        '<img src="https://firebasestorage.googleapis.com/v0/b/histo-vet.appspot.com/o/logo3.png?alt=media&token=7d4e6506-3456-4e39-9c9c-4960a8c3d13f">'
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
          res.status(500).send(error.message);
      }
      console.log('Message %s sent: %s', info.messageId, info.response);
  });

  res.status(200).send("Solicitud de cita exitosa!");
});

app.use(router);

server.listen(8080, function(){
  console.log('Servidor corriendo en http://localhost:8080');
});
