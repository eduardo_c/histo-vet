var selectCita, selectCollect, selectTipoCita, selectPuncion, selectPuncionClinica;

function encodeEmail(email){
  //return email.replace('.',',');
  return email.split('.').join(',');
}

$(document).ready(function() {

  var _sectionCitas = document.getElementById('seccion_citas');
  var _sectionContact = document.getElementById('seccion_contact');
  var _slide1, _slide2, _slide3, _slide4, _slide5, _navHeight;
  var _msg1;
  var _config = {attributes: true, childList: true, characterData: true};
  var requestingService = false;
  var selectedDate;
  var serviceType, serviceType2, serviceType3;

  $("#seccion_inicio").load("inicio.html", function(){
    $('#fullpage').fullpage({
      sectionsColor: ['#1bbc9b', '#ffffff', '#f7f7f7', '#3283c4', '#7BAABE', 'whitesmoke', '#ccddff'],
      anchors: ['Home', 'Nosotros', 'Servicios', 'Cita', 'Resultados', 'Contacto'],
      menu: '#nav_menu'
    });

    _navHeight = $('#nav').height();
    $("#seccion1_logo").css({top: (_navHeight + 40) + 'px'});

    _slide1 = document.getElementById('slide1');
    _slide2 = document.getElementById('slide2');
    _slide3 = document.getElementById('slide3');
    _slide4 = document.getElementById('slide4');
    _slide5 = document.getElementById("slide5");
    _msg1 = document.getElementById('msg1');

    var observer = new MutationObserver(function(mutations){
      mutations.forEach(function(mutation){
        console.log(mutation.target.id);
        switch (mutation.target.id) {
          case 'seccion_citas':
            if(_sectionCitas.classList.contains('active'))
              $('#form_citas_title p').addClass('fadeIn');
              //$('#date_form_content').addClass('form_loaded');
            break;
          case 'slide2':
              $('#msg1').addClass('fadeIn');
            break;
          case 'slide3':
              $('#msg2').addClass('fadeIn');
            break;
          case 'slide4':
              $('#msg3').addClass('fadeIn');
            break;
          case 'slide5':
              $('#msg4').addClass('fadeIn');
            break;
          case 'seccion_contact':
              if(_sectionContact.classList.contains('active')){
                $('#contact_info').addClass('slideLeft');
                $('#contact_map').addClass('bigEntrance');
                $('#fb').addClass('bigEntrance');
                $('#twitter').addClass('bigEntrance');
                $('#insta').addClass('bigEntrance');
              }
            break;
        }
      });
    });
    observer.observe(_slide2, _config);
    observer.observe(_slide3, _config);
    observer.observe(_slide4, _config);
    observer.observe(_slide5, _config);
    observer.observe(_sectionCitas, _config);
    observer.observe(_sectionContact, _config);
  });

  $("#seccion_nosotros").load("nosotros.html", function(){
    if(navigator.userAgent.indexOf("Firefox") != -1){
      $('#seccion_nosotros').css({paddingTop: (_navHeight + 80) + 'px'});
      $('#separador').css({width: '40px'});
      $('#partners').css({marginTop: '3%'});
    }
    else
      $('#seccion_nosotros').css({paddingTop: (_navHeight + 40) + 'px'});
  });

  $("#seccion_servicios").load("servicios.html", function(){
    if(navigator.userAgent.indexOf("Firefox") != -1){
      $('#seccion_servicios').css({paddingTop: (_navHeight + 30) + 'px'});
      $('#services_constraints p').css({marginBottom: "1%"});
      $('#seccion_servicios .details p').css({fontSize: '0.8em !important'});
      $('#seccion_servicios h3').css({fontSize: '1.2em'});
      $('p.description').css({fontSize: '1em'});
    }
    else
      $('#seccion_servicios').css({paddingTop: (_navHeight + 40) + 'px'});
  });

    $("#seccion_citas").load("citas.html", function() {

      if(navigator.userAgent.indexOf('Firefox') != -1){
        $('#form_citas_title').css({position: 'absolute'});
        $('#date_form_content').css({position: 'absolute'});
        $('#date_form_content').css({right: '0'});
      }

      $.datepicker.regional.es = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
      };
      $.datepicker.setDefaults($.datepicker.regional.es);

      $( "#datepicker" ).datepicker({
        minDate: 0,
        maxDate: "5M +10D",
        dateFormat: 'yy-mm-dd',
        onSelect: function(){
          selectedDate = $(this).datepicker('getDate');
        }
      });

      $("#select_cita").change(function() {
        serviceType2 = undefined;
        serviceType3 = undefined;
        switch (this.value) {
          case "collect":
          serviceType = 'Recolección de muestra';
            $('#cita').css('display', 'none');
            $('#cita').css('opacity', '0');
            $('#puncion').css('display', 'none');
            $('#puncion').css('opacity', '0');
            $('#collect').css('display', 'block');
            $('#collect').css('opacity', '1');
            $('#puncion_clinica').css('display', 'none');
            $('#puncion_clinica').css('opacity', '0');
            break;
          case "date":
            serviceType = 'Cita para toma de muestra en clínica/hospital';
            $('#cita').css('display', 'block');
            $('#cita').css('opacity', '1');
            $('#puncion').css('display', 'none');
            $('#puncion').css('opacity', '0');
            $('#collect').css('display', 'none');
            $('#collect').css('opacity', '0');
            $('#puncion_clinica').css('display', 'none');
            $('#puncion_clinica').css('opacity', '0');
            break;
          case "puncion":
          serviceType = 'Punción guiada c/ultrasonido';
            $('#cita').css('display', 'none');
            $('#cita').css('opacity', '0');
            $('#puncion').css('display', 'block');
            $('#puncion').css('opacity', '1');
            $('#collect').css('display', 'none');
            $('#collect').css('opacity', '0');
          break;
          default:

        }
      });

      $('#select_collect').change(function(){
        serviceType3 = undefined;
        switch (this.value) {
          case 'cito':
            serviceType2 = 'Citopatología';
            break;
          case 'bio':
            serviceType2 = 'Biopsía';
            break;
          case 'necro':
            serviceType2 = 'Necropsia';
            break;
          default:
            serviceType2 = 'Tipo no especificado';
        }
      });

      $('#select_tipo_cita').change(function(){
        switch ($(this).value) {
          case "uno":
            serviceType2 = ' Aspiración con aguja fina, 1 sitio anatómico';
            break;
          case "dos":
            serviceType2 = ' Aspiración con aguja fina, 2 sitios o más';
            break;
          default:
            serviceType2 = ' Tipo no especificado';
        }
      });

      $('#select_puncion').change(function(){
        switch (this.value) {
          case "clinica":
            serviceType2 = 'Visita a la clínica/ hospital del M.V.Z. (solicitante):';
            $('#puncion_clinica').css('display', 'block');
            $('#puncion_clinica').css('opacity', '1');
            break;
          case "laboratorio":
            serviceType2 = 'Visita al laboratorio de Histovet Yucatán';
            $('#puncion_clinica').css('display', 'none');
            $('#puncion_clinica').css('opacity', '0');
            break;
          default:

        }
      });

      $('#select_puncion_clinica').change(function(){
        switch ($(this).value) {
          case 'sedacion':
            serviceType3 = ' Con sedación/anestesia a cargo del M.V.Z';
            break;
          case 'equipo':
            serviceType3 = ' Con equipo de US a cargo del patólogo.';
            break;
          default:

        }
      });

      $('#citas_content .btn_date').click(function(){
        $("#logout_pd").css("display", "block");
        requestingService = true;
        firebase.auth().onAuthStateChanged(function(user){
          if(requestingService){
            requestingService = false;
            if(user){
              //alert("Solicitar cita:\n" + selectedDate + '\n' + serviceType + serviceType2 + serviceType3);
              if(serviceType == undefined || serviceType2 == undefined)
                alert('Debes seleccionar un tipo de cita y la fecha deseada');
              else if(selectedDate == undefined)
                alert('Debes seleccionar una fecha para la cita');
              else{
                var userMail = user.email;
                var userRef = database.ref('users/' + encodeEmail(userMail));
                var username, userphone;
                userRef.once('value', function(snapshot){
                  if(snapshot){
                    username = snapshot.val().username;
                    userphone = snapshot.val().celular;
                    var reqBody = {userName: username, userPhone: userphone, userEmail: userMail, date: selectedDate, st: serviceType, st2: serviceType2, st3: serviceType3 };
                    $.post('http://localhost:8080/service_request', reqBody, function(data, status){
                      alert(data);
                    });
                  }
                });
              }
            }
            else{
              alert('Debes iniciar sesión');
            }
          }
        });
        $("#logout_pd").css("display", "none");
      });
    });

    $("#seccion_resultados").load("results.html", function() {
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          var userMail = user.email;
          var userRef = database.ref('users/' + encodeEmail(userMail));
          console.log(userRef);
          userRef.once('value', function(snapshot){
            if(snapshot.val().resultado){
              $('#no_results').replaceWith('<a id="download_results" class="btn_date" href="' +
              snapshot.val().resultado +
              '" download><p>Descargar Resultado</p><img src="./img/download.png"></a>');
              $('#results_container').append('<p id="results_constraints">Los diagnósticos resultados se descargan en \
              formato PDF y estarán disponibles 15 días, posteriormente serán eliminados del portal.</p>')
              /*$('#no_results').attr('id','results');
              $('#no_results_msg').replaceWith('<p id="no_results_msg" class="no-select">Descargar Resultados</p>');
              $('#results img').replaceWith('<a href="'+ snapshot.val().resultado +'" download><img src="./img/download.png"></a>');*/
            }
            else{
              //El usuario esta logeado pero no tiene resultados disponibles
              $('#no_results img').css('display', 'block');
            }
          });
        }
        else{
          //Usuario no logeado
          $('#no_results_msg').replaceWith('<p id="no_results_msg" class="no-select">Debes iniciar sesión para consultar resultados</p>');
          $('#no_results img').css('display', 'none');
        }
      });
    });

    $("#seccion_contact").load("contact.html", function(){
      if(navigator.userAgent.indexOf('Firefox') != -1)
        $('#contact_constraint').css({marginTop: '27%'});
    });

    window.onresize = function(event){
      _navHeight = $('#nav').height();
      $("#seccion1_logo").css({top: (_navHeight + 40) + 'px'});
      //$("#citas_content").style.paddingTop = (_navHeight + 40) + 'px';
      if(navigator.userAgent.indexOf("Firefox") != -1)
        $('#seccion_nosotros').css({paddingTop: (_navHeight + 80) + 'px'});
      else
        $('#seccion_nosotros').css({paddingTop: (_navHeight + 40) + 'px'});
      $('#seccion_servicios').css({paddingTop: (_navHeight + 40) + 'px'});
    };
});
