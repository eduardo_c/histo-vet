$(function(){
  var logingOut = false;

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      $('#login_button_wrapp').empty().append("<a id='login_button'>Cerrar Sesión</a>");
    } else {
      $('#login_button_wrapp').empty().append("<a id='login_button'>Iniciar Sesión</a>");
    }
  });

    $("#home").addClass("active");

  $("li").click(function(){
    if(this.className == "active"){
      //this.classList.remove("active");
    } else {
        //$("li.active").classList.remove("active");
        $("li.active").removeClass("active");
      this.className = "active";
    }
});

  $("#login_button_wrapp").click(function(){
    $("#logout_pd").css("display", "block");
    firebase.auth().onAuthStateChanged(function(user) {
      if (user) {
        //Cerrar sesión del usuario
        firebase.auth().signOut().then(function(){
          logingOut = true;
        }).catch(function(error){
          console.log(error);
        });
      } else {
        //Nos situamos en la pagina de inicio de sesión
        if(logingOut){
          logingOut = false;
          window.location = "http://localhost:8080/index.html";
        }
        else
          window.location = "http://localhost:8080/home.html";
      }

      $("#logout_pd").css("display", "none");
    });
  });
});
