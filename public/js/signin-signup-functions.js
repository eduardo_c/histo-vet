function encodeEmail(email){
  return email.replace('.',',');
}

function registerNewUser(email, pass){
  firebase.auth().createUserWithEmailAndPassword(email, pass)
  .then(function(userRecord) {
    console.log('Registro exitoso de nuevo usuario!');
    //var email = $("#email").val();
    var name = $("#user_register").val();
    var tel = $("#tel").val();
    var cel = $("#cel").val();

    writeUserData(email, name, tel, cel);
  })
  .catch(function(error) {
    console.log("Error en registro de nuevo usuario");
    var error = error.message;
    alert(error);
  });
}

function writeUserData(email, name, tel, cel){
  database.ref('users/' + encodeEmail(email)).set({
    username: name,
    telefono: tel,
    celular: cel
  }).then(function(record){
    console.log('Registro en DB exitoso!')
  }).catch(function(error) {
    console.log("Error de registro en DB");
  });
}

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    window.location="http://localhost:8080/index.html";
    $("#login_pd").css("display", "none");
    $("#signup_pd").css("display", "none");
  }
});

$(function(){

  $("#btn_register").click(function() {
    var email = $("#email").val();
    var pass = $("#pass_register").val();
    var pass2 = $("#pass_repeat").val();
    if(pass == pass2){
      registerNewUser(email, pass);
      $("#signup_pd").css("display", "block");
    }
    else {
      alert("La contraseña no coincide")
    }
  });

  $("#btn_login").click(function() {
    $("#login_pd").css("display", "block");
    var user = $("#user").val();
    var pass = $("#pass").val();
    firebase.auth().signInWithEmailAndPassword(user, pass).catch(function(error) {
      $("#login_pd").css("display", "none");
      console.log(error.message);
      alert(error.message);
    });
  });

});
